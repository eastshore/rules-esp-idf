load('@bazel_tools//tools/build_defs/repo:http.bzl', 'http_file', 'http_archive')

def espressif_gcc_dependency(version, target_platform, host_platform, sha256, build_file):
    http_archive(
        name = "gcc-espressif-{}-{}".format(target_platform, host_platform),
        urls = [
            "https://github.com/espressif/crosstool-NG/releases/download/esp-{version}/xtensa-{target}-elf-{version}-{host}.tar.xz".format(
                version = version,
                target = target_platform,
                host = host_platform,
            ),
        ],
        sha256 = sha256,
        strip_prefix = "xtensa-{}-elf".format(target_platform),
        build_file = build_file,
    )

VERSION = "12.2.0_20230208"

def espressif_gcc_esp32_dependencies():
    espressif_gcc_dependency(
        version = VERSION,
        target_platform = "esp32",
        host_platform = "x86_64-linux-gnu",
        sha256 = "4d2e02ef47f1a93a4dcfdbaecd486adfaab4c0e26deea2c18d6385527f39f864",
        build_file = "@rules_esp-idf//toolchain/esp32:xtensa-gcc-linux-x86_64.bzl",
    )

    espressif_gcc_dependency(
        version = VERSION,
        target_platform = "esp32",
        host_platform = "aarch64-linux-gnu",
        sha256 = "9e211a182b6ea0396a41c78f52f51d964e7875fe274ea9c81111bf0dbc90c516",
        build_file = "@rules_esp-idf//toolchain/esp32:xtensa-gcc-linux-aarch64.bzl",
    )

    espressif_gcc_dependency(
        version = VERSION,
        target_platform = "esp32",
        host_platform = "arm-linux-gnueabi",
        sha256 = "2ddd91fb98b79b30042b7918eef60cf10c7bd5b1da853e83b65f293b96dec800",
        build_file = "@rules_esp-idf//toolchain/esp32:xtensa-gcc-linux-arm.bzl",
    )

    espressif_gcc_dependency(
        version = VERSION,
        target_platform = "esp32",
        host_platform = "arm-linux-gnueabihf",
        sha256 = "a683a468555dcbcb6ce32a190842110d6f853d4d6104d61cf0bc9dd50c6be1e6",
        build_file = "@rules_esp-idf//toolchain/esp32:xtensa-gcc-linux-armhf.bzl",
    )

    espressif_gcc_dependency(
        version = VERSION,
        target_platform = "esp32",
        host_platform = "i686-linux-gnu",
        sha256 = "292b19ea6186508a923fb6fd0103977e001d4eb8e77836c7e3d6ce6e5fa7d305",
        build_file = "@rules_esp-idf//toolchain/esp32:xtensa-gcc-linux-x86.bzl",
    )

    espressif_gcc_dependency(
        version = VERSION,
        target_platform = "esp32",
        host_platform = "x86_64-apple-darwin",
        sha256 = "b09d87fdb1dc32cd1d718935065ef931b101a14df6b17be56748e52640955bff",
        build_file = "@rules_esp-idf//toolchain/esp32:xtensa-gcc-macos-x86_64.bzl",
    )

    espressif_gcc_dependency(
        version = VERSION,
        target_platform = "esp32",
        host_platform = "arm64-apple-darwin",
        sha256 = "f50acab2b216e9475dc5313b3e4b424cbc70d0abd23ba1818aff4a019165da8e",
        build_file = "@rules_esp-idf//toolchain/esp32:xtensa-gcc-macos-arm64.bzl",
    )

    espressif_gcc_dependency(
        version = VERSION,
        target_platform = "esp32",
        host_platform = "i686-w64-mingw32",
        sha256 = "62bb6428d107ed3f44c212c77ecf24804b74c97327b0f0ad2029c656c6dbd6ee",
        build_file = "@rules_esp-idf//toolchain/esp32:xtensa-gcc-windows-x86.bzl",
    )

    espressif_gcc_dependency(
        version = VERSION,
        target_platform = "esp32",
        host_platform = "x86_64-w64-mingw32",
        sha256 = "8febfe4a6476efc69012390106c8c660a14418f025137b0513670c72124339cf",
        build_file = "@rules_esp-idf//toolchain/esp32:xtensa-gcc-windows-x86_64.bzl",
    )

def espressif_gcc_dependencies():
    espressif_gcc_esp32_dependencies()
