# toolchains/compiler.BUILD

package(default_visibility = ['//visibility:public'])

# export the executable files to make them available for direct use.
exports_files(glob(["bin/*"]))

# gcc executables.
filegroup(
    name = "gcc",
    srcs = glob([
        "bin/xtensa-esp32-elf-gcc.exe",
    ]),
)

# ld executables.
filegroup(
    name = "ld",
    srcs = glob([
        "bin/xtensa-esp32-elf-ld.exe",
    ]),
)

# nm executables.
filegroup(
    name = "nm",
    srcs = glob([
        "bin/xtensa-esp32-elf-nm.exe",
    ]),
)

# objcopy executables.
filegroup(
    name = "objcopy",
    srcs = glob([
        "bin/xtensa-esp32-elf-objcopy.exe",
    ]),
)

# objdump executables.
filegroup(
    name = "objdump",
    srcs = glob([
        "bin/xtensa-esp32-elf-objdump.exe",
    ]),
)

# strip executables.
filegroup(
    name = "strip",
    srcs = glob([
        "bin/xtensa-esp32-elf-strip.exe",
    ]),
)

# as executables.
filegroup(
    name = "as",
    srcs = glob([
        "bin/xtensa-esp32-elf-as.exe",
    ]),
)

# ar executables.
filegroup(
    name = "ar",
    srcs = glob([
        "bin/xtensa-esp32-elf-ar.exe",
    ]),
)

# size executables.
filegroup(
    name = "size",
    srcs = glob([
        "bin/xtensa-esp32-elf-size.exe",
    ]),
)


# cc1 executable
filegroup(
    name = "cc1",
    srcs = glob([
        "libexec/gcc/xtensa-esp32-elf/12.2.0/cc1.exe",
    ]),
)

# libraries and headers.
filegroup(
    name = "compiler_pieces",
    srcs = glob([
        "xtensa-esp32-elf/**",
        "lib/gcc/xtensa-esp32-elf/**",
    ]),
)

# collection of executables.
filegroup(
    name = "compiler_components",
    srcs = [
        ":ar",
        ":as",
        ":gcc",
        ":ld",
        ":nm",
        ":objcopy",
        ":objdump",
        ":strip",
        ":cc1",
    ],
)

filegroup(
    name = "includes",
    srcs = glob([
        "xtensa-esp32-elf/include/*.h",
        "xtensa-esp32-elf/include/**/*.h",
        "xtensa-esp32-elf/sys-include/*.h",
        "xtensa-esp32-elf/sys-include/**/*.h",
        "xtensa-esp32-elf/include/c++/12.2.0/*.h",
        "xtensa-esp32-elf/include/c++/12.2.0/**/*.h",
        "lib/gcc/xtensa-esp32-elf/12.2.0/include/*.h",
        "lib/gcc/xtensa-esp32-elf/12.2.0/include/**/*.h",
        "xtensa-esp32-elf/include/c++/12.2.0/xtensa-esp32-elf/esp32-psram/*/*.h",
        "xtensa-esp32-elf/include/c++/12.2.0/xtensa-esp32-elf/*/*.h"
    ]),
)

filegroup(
    name = "linker-includes",
    srcs = glob([
        "xtensa-esp32-elf/lib/*.a",
        "xtensa-esp32-elf/lib/**/*.a",
        "lib/gcc/xtensa-esp32-elf/12.2.0/*.a",
        "lib/gcc/xtensa-esp32-elf/12.2.0/**/*.a",
    ]),
)

load("@rules_esp-idf//toolchain/esp32:config.bzl", "cc_xtensa_config")

IDENTIFIER = "gcc-xtensa-esp32-elf-windows-x86_64"

cc_xtensa_config(
    name = "config",
    gcc = "bin/xtensa-esp32-elf-gcc.exe",
    ld = "bin/xtensa-esp32-elf-ld.exe",
    ar = "bin/xtensa-esp32-elf-ar.exe",
    cpp = "bin/xtensa-esp32-elf-cpp.exe",
    gcov = "bin/xtensa-esp32-elf-gcov.exe",
    nm = "bin/xtensa-esp32-elf-nm.exe",
    objdump = "bin/xtensa-esp32-elf-objdump.exe",
    strip = "bin/xtensa-esp32-elf-strip.exe",
    cc1 = "libexec/gcc/xtensa-esp32-elf/12.2.0/cc1.exe",
    compiler_name = IDENTIFIER,
    toolchain_identifier = IDENTIFIER,
    host_system_name = "windows-x86_64",
    visibility = [ "//visibility:public" ],
)

filegroup(
    name = "compiler_files",
    srcs = [
        ":compiler_pieces",
        ":gcc",
        ":cc1",
    ],
)

filegroup(
    name = "linker_files",
    srcs = [
        ":ar",
        ":compiler_pieces",
        ":gcc",
        ":ld",
        ":cc1",
    ]
)

filegroup(
    name = "all_files",
    srcs = [
        ":compiler_files",
        ":linker_files",
    ],
)

cc_toolchain(
    name ="cc_toolchain",
    all_files = ":all_files",
    ar_files = ":ar",
    compiler_files = ":compiler_files",
    dwp_files = ":empty",
    linker_files = ":linker_files",
    objcopy_files = ":objcopy",
    strip_files = ":strip",
    supports_param_files = 1,
    toolchain_config = ":config",
    toolchain_identifier = IDENTIFIER,
    visibility = [ "//visibility:public" ],
)
