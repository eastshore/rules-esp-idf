# toolchains/compiler.BUILD

package(default_visibility = ['//visibility:public'])

# export the executable files to make them available for direct use.
exports_files(glob(["bin/*"]))

# gcc executables.
filegroup(
    name = "gcc",
    srcs = glob([
        "bin/xtensa-esp32-elf-gcc",
    ]),
)

# ld executables.
filegroup(
    name = "ld",
    srcs = glob([
        "bin/xtensa-esp32-elf-ld",
    ]),
)

# nm executables.
filegroup(
    name = "nm",
    srcs = glob([
        "bin/xtensa-esp32-elf-nm",
    ]),
)

# objcopy executables.
filegroup(
    name = "objcopy",
    srcs = glob([
        "bin/xtensa-esp32-elf-objcopy",
    ]),
)

# objdump executables.
filegroup(
    name = "objdump",
    srcs = glob([
        "bin/xtensa-esp32-elf-objdump",
    ]),
)

# strip executables.
filegroup(
    name = "strip",
    srcs = glob([
        "bin/xtensa-esp32-elf-strip",
    ]),
)

# as executables.
filegroup(
    name = "as",
    srcs = glob([
        "bin/xtensa-esp32-elf-as",
    ]),
)

# ar executables.
filegroup(
    name = "ar",
    srcs = glob([
        "bin/xtensa-esp32-elf-ar",
    ]),
)

# size executables.
filegroup(
    name = "size",
    srcs = glob([
        "bin/xtensa-esp32-elf-size",
    ]),
)


# cc1 executable
filegroup(
    name = "cc1",
    srcs = glob([
        "libexec/gcc/xtensa-esp32-elf/12.2.0/cc1",
    ]),
)

# libraries and headers.
filegroup(
    name = "compiler_pieces",
    srcs = glob([
        "xtensa-esp32-elf/**",
        "lib/gcc/xtensa-esp32-elf/**",
    ]),
)

# collection of executables.
filegroup(
    name = "compiler_components",
    srcs = [
        ":ar",
        ":as",
        ":gcc",
        ":ld",
        ":nm",
        ":objcopy",
        ":objdump",
        ":strip",
        ":cc1",
    ],
)

filegroup(
    name = "includes",
    srcs = glob([
        "xtensa-esp32-elf/include/*.h",
        "xtensa-esp32-elf/include/**/*.h",
        "xtensa-esp32-elf/sys-include/*.h",
        "xtensa-esp32-elf/sys-include/**/*.h",
        "xtensa-esp32-elf/include/c++/12.2.0/*.h",
        "xtensa-esp32-elf/include/c++/12.2.0/**/*.h",
        "lib/gcc/xtensa-esp32-elf/12.2.0/include/*.h",
        "lib/gcc/xtensa-esp32-elf/12.2.0/include/**/*.h",
        "xtensa-esp32-elf/include/c++/12.2.0/xtensa-esp32-elf/esp32-psram/*/*.h",
        "xtensa-esp32-elf/include/c++/12.2.0/xtensa-esp32-elf/*/*.h"
    ]),
)

filegroup(
    name = "linker-includes",
    srcs = glob([
        "xtensa-esp32-elf/lib/*.a",
        "lib/gcc/xtensa-esp32-elf/12.2.0/*.a",
    ]),
)

load("@rules_esp-idf//toolchain/esp32:config.bzl", "cc_xtensa_config")

IDENTIFIER = "gcc-xtensa-esp32-elf-macos-arm64"

cc_xtensa_config(
    name = "config",
    gcc = "bin/xtensa-esp32-elf-gcc",
    ld = "bin/xtensa-esp32-elf-ld",
    ar = "bin/xtensa-esp32-elf-ar",
    cpp = "bin/xtensa-esp32-elf-cpp",
    gcov = "bin/xtensa-esp32-elf-gcov",
    nm = "bin/xtensa-esp32-elf-nm",
    objdump = "bin/xtensa-esp32-elf-objdump",
    strip = "bin/xtensa-esp32-elf-strip",
    cc1 = "libexec/gcc/xtensa-esp32-elf/12.2.0/cc1",
    compiler_name = IDENTIFIER,
    toolchain_identifier = IDENTIFIER,
    host_system_name = "macos-arm64",
    visibility = [ "//visibility:public" ],
)

filegroup(
    name = "compiler_files",
    srcs = [
        ":compiler_pieces",
        ":gcc",
        ":cc1",
    ],
)

filegroup(
    name = "linker_files",
    srcs = [
        ":ar",
        ":compiler_pieces",
        ":gcc",
        ":ld",
        ":cc1",
    ]
)

filegroup(
    name = "all_files",
    srcs = [
        ":compiler_files",
        ":linker_files",
    ],
)

cc_toolchain(
    name ="cc_toolchain",
    all_files = ":all_files",
    ar_files = ":ar",
    compiler_files = ":compiler_files",
    dwp_files = ":empty",
    linker_files = ":linker_files",
    objcopy_files = ":objcopy",
    strip_files = ":strip",
    supports_param_files = 1,
    toolchain_config = ":config",
    toolchain_identifier = IDENTIFIER,
    visibility = [ "//visibility:public" ],
)
