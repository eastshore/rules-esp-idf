
def register_toolchains():
    native.register_toolchains(
        "@rules_esp-idf//toolchain/esp32:linux-amd64",
        "@rules_esp-idf//toolchain/esp32:linux-aarch64",
        "@rules_esp-idf//toolchain/esp32:linux-arm",
        "@rules_esp-idf//toolchain/esp32:linux-armhf",
        "@rules_esp-idf//toolchain/esp32:linux-i686",
        "@rules_esp-idf//toolchain/esp32:macos-arm64",
        "@rules_esp-idf//toolchain/esp32:macos-amd64",
        "@rules_esp-idf//toolchain/esp32:windows-amd64",
        "@rules_esp-idf//toolchain/esp32:windows-i686",
    )