load("@bazel_tools//tools/build_defs/cc:action_names.bzl", "ACTION_NAMES")
load("@bazel_tools//tools/cpp:cc_toolchain_config_lib.bzl", "feature", "flag_group", "flag_set", "tool_path")

GCC_VERSION = "12.2.0"

_attrs = {
    "gcc": attr.string(mandatory = True),
    "ld": attr.string(mandatory = True),
    "ar": attr.string(mandatory = True),
    "cpp": attr.string(mandatory = True),
    "gcov": attr.string(mandatory = True),
    "nm": attr.string(mandatory = True),
    "objdump": attr.string(mandatory = True),
    "strip": attr.string(mandatory = True),
    "cc1": attr.string(mandatory = True),
    "toolchain_identifier": attr.string(mandatory = True),
    "compiler_name": attr.string(mandatory = True),
    "host_system_name": attr.string(mandatory = True),
    "gcc_version": attr.string(),
}

def _flatten_list(list):
    return [ item for sublist in list for item in sublist ]

def _impl(ctx):
    tool_paths = [
        tool_path(name = "gcc", path = ctx.attr.gcc),
        tool_path(name = "ld", path = ctx.attr.ld),
        tool_path(name = "ar", path = ctx.attr.ar),
        tool_path(name = "cpp", path = ctx.attr.cpp),
        tool_path(name = "gcov", path = ctx.attr.gcov),
        tool_path(name = "nm", path = ctx.attr.nm),
        tool_path(name = "objdump", path = ctx.attr.objdump),
        tool_path(name = "strip", path = ctx.attr.strip),
        tool_path(name = "gcc1", path = ctx.attr.gcc),
    ]

    C_INCLUDES = [
        #"external/gcc-espressif-esp32-{}/xtensa-esp32-elf/include".format(ctx.attr.host_system_name),
        "%package(@gcc-espressif-esp32-{}//)%/xtensa-esp32-elf/include".format(ctx.attr.host_system_name),
        #"%package(@gcc-espressif-esp32-{}///xtensa-esp32-elf/include)%".format(ctx.attr.host_system_name),
        #"xtensa-esp32-elf/include",
        #"external/gcc-espressif-esp32-{}/xtensa-esp32-elf/sys-include".format(ctx.attr.host_system_name),
        "%package(@gcc-espressif-esp32-{}//)%/xtensa-esp32-elf/sys-include".format(ctx.attr.host_system_name),
        #"%package(@gcc-espressif-esp32-{}///xtensa-esp32-elf/sys-include)%".format(ctx.attr.host_system_name),
        #"xtensa-esp32-elf/sys-include",
        #"external/gcc-espressif-esp32-{}/lib/gcc/xtensa-esp32-elf/{}/include".format(
        "%package(@gcc-espressif-esp32-{}//)%/lib/gcc/xtensa-esp32-elf/{}/include".format(
            ctx.attr.host_system_name,
            GCC_VERSION
        ),
        #"%package(@gcc-espressif-esp32-{}///lib/gcc/xtensa-esp32-elf/{}/include)%".format(
        #    ctx.attr.host_system_name,
        #    GCC_VERSION
        #),
        #"lib/gcc/xtensa-esp32-elf/{}#/include".format(
        #    GCC_VERSION
        #),
    ]

    CPP_INCLUDES = [
        #"external/gcc-espressif-esp32-{}/xtensa-esp32-elf/include/c++/{}/".format(
        #"%package(@gcc-espressif-esp32-{}//)%/xtensa-esp32-elf/include/c++/{}".format(
        #    ctx.attr.host_system_name,
        #    GCC_VERSION,
        #),
        "xtensa-esp32-elf/include/c++/{}".format(
            GCC_VERSION,
        ),
        "xtensa-esp32-elf/include/c++/{}/xtensa-esp32-elf/esp32-psram".format(
            GCC_VERSION,
        ),
        "xtensa-esp32-elf/include/c++/{}/xtensa-esp32-elf".format(
            GCC_VERSION,
        ),
    ]

    toolchain_compiler_flags = feature(
        name = "compiler_flags",
        enabled = True,
        flag_sets = [
            flag_set(
                actions = [
                    ACTION_NAMES.assemble,
                    ACTION_NAMES.preprocess_assemble,
                    ACTION_NAMES.linkstamp_compile,
                    ACTION_NAMES.c_compile,
                    ACTION_NAMES.cpp_compile,
                    ACTION_NAMES.cpp_header_parsing,
                    ACTION_NAMES.cpp_module_compile,
                    ACTION_NAMES.cpp_module_codegen,
                    ACTION_NAMES.lto_backend,
                    ACTION_NAMES.clif_match,
                ],
                flag_groups = [
                    #flag_group(flags = ["-isystem " + path for path in C_INCLUDES]),
                    flag_group(flags = [
                        "-fno-canonical-system-headers",
                        "-no-canonical-prefixes",
                    ]),
                ],
            ),
             flag_set(
                actions = [
                    ACTION_NAMES.assemble,
                    ACTION_NAMES.preprocess_assemble,
                    ACTION_NAMES.linkstamp_compile,
                    ACTION_NAMES.cpp_compile,
                    ACTION_NAMES.cpp_header_parsing,
                    ACTION_NAMES.cpp_module_compile,
                    ACTION_NAMES.cpp_module_codegen,
                    ACTION_NAMES.lto_backend,
                    ACTION_NAMES.clif_match,
                ],
                flag_groups = [
                    #flag_group(flags = ["-isystem " + path for path in CPP_INCLUDES ]),
                    flag_group(flags = [
                        "-fno-canonical-system-headers",
                        "-no-canonical-prefixes",
                    ]),
                ],
            ),
        ],
    )
    toolchain_linker_flags = feature(
        name = "linker_flags",
        enabled = True,
        flag_sets = [
            flag_set(
                actions = [
                    ACTION_NAMES.linkstamp_compile,
                    ACTION_NAMES.cpp_link_executable,
                    ACTION_NAMES.cpp_link_dynamic_library,
                    ACTION_NAMES.cpp_link_nodeps_dynamic_library,
                    ACTION_NAMES.cpp_link_static_library,
                ],
                flag_groups = [
                    flag_group(flags = [
                        "--specs=nosys.specs",
                        "-L external/gcc-espressif-esp32-{}/xtensa-esp32-elf/lib/".format(
                            ctx.attr.host_system_name,
                        ),
                        "-L external/gcc-espressif-esp32-{}/lib/gcc/xtensa-esp32-elf/{}/".format(
                            ctx.attr.host_system_name,
                            GCC_VERSION,
                        ),
                    ]),
                ],
            ),
        ],
    )


    return cc_common.create_cc_toolchain_config_info(
        ctx = ctx,
        toolchain_identifier = ctx.attr.toolchain_identifier,
        target_system_name = "xtensa-esp32-elf",
        host_system_name = ctx.attr.host_system_name,
        target_cpu = "xtensa-esp32-elf",
        target_libc = "gcc",
        compiler = ctx.attr.compiler_name,
        abi_version = ctx.attr.gcc_version,
        cxx_builtin_include_directories = C_INCLUDES + CPP_INCLUDES,
        #[
        #    "xtensa-esp32-elf/include",
        #    "xtensa-esp32-elf/sys-include",
        #    "xtensa-esp32-elf/include/c++/{}/*".format(GCC_VERSION),
        #    "lib/gcc/xtensa-esp32-elf/{}/include".format(GCC_VERSION),
        #],
        tool_paths = tool_paths,
        features = [
            toolchain_compiler_flags,
            toolchain_linker_flags,
        ],
    )

cc_xtensa_config = rule(
    implementation = _impl,
    attrs = _attrs,
    provides = [CcToolchainConfigInfo],
)
